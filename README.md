# Recruitment Task

<p>Before usage, please fill docker-compose parameter -<b>API_KEY</b>- key needed to use omdbapi</p>
<p>Field <b>JWT_SECRET</b> inside docker-compose has default value as recruitment-task - secret</p>
<p>To start the server, please write in terminal (while beeing inside project) <b>docker-compose up --build</b></p>
<p>To run tests locally, please write <b>./tests.sh</b> in terminal</p>
