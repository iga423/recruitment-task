FROM node:14.15-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install && apk update && apk add bash

COPY . .

EXPOSE 8080

CMD ["npm", "start"]
