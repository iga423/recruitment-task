class OmdbApiModule {
  static async getMovieInfo(movieTitle) {
    if (movieTitle === "little") {
      return {
        Title: "Little Miss Sunshine",
        Genre: "Comedy, Drama",
        Director: "Jonathan Dayton, Valerie Faris",
        Released: new Date(),
      };
    } else return { Response: "False" };
  }
}
module.exports = OmdbApiModule;
