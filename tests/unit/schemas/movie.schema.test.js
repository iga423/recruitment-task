const { expect } = require("chai");
const MovieSchema = require("../../../src/schemas/movie.schema");

describe("movie schema tests", function () {
  afterEach(async () => {
    await MovieSchema.remove();
  });
  it("should successfully save new movie", async () => {
    //Arrange
    const movie = {
      title: "title",
      released: new Date(),
      genre: "genre",
      director: "director",
      createdBy: "12323",
    };

    //Act
    const newMovie = new MovieSchema(movie);
    await newMovie.save();

    //Assert
    const savedMovie = await MovieSchema.findOne({ title: "title" });
    const savedMovieDate = new Date(savedMovie.released);

    expect(savedMovie.title).to.equal(movie.title);
    expect(savedMovie.genre).to.equal(movie.genre);
    expect(savedMovie.director).to.equal(movie.director);
    expect(savedMovie.createdBy).to.equal(movie.createdBy);
    expect(savedMovieDate.getFullYear()).to.equal(movie.released.getFullYear());
    expect(savedMovieDate.getDay()).to.equal(movie.released.getDay());
    expect(savedMovieDate.getMonth()).to.equal(movie.released.getMonth());
  });
});
