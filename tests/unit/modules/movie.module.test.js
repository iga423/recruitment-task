const MovieSchema = require("../../../src/schemas/movie.schema");
const MovieModule = require("../../../src/modules/movie.module");
const { expect } = require("chai");

describe("movie module tests", function () {
  afterEach(async () => {
    await MovieSchema.remove();
  });
  it("should return false if user already saved 5 movies in one month", async () => {
    //Arrange
    const movie = {
      title: "title",
      released: new Date(),
      genre: "genre",
      director: "director",
      createdBy: "12323",
    };

    //Act
    for (let i = 0; i < 6; i++) {
      const newMovie = new MovieSchema(movie);
      await newMovie.save();
    }

    //Assert
    expect(await MovieModule.canUserAddMovies("12323")).to.be.false;
  });

  it("should return true if user saved less than 5 movies in one month", async () => {
    //Arrange
    const movie = {
      title: "title",
      released: new Date(),
      genre: "genre",
      director: "director",
      createdBy: "12323",
    };

    //Act
    const newMovie = new MovieSchema(movie);
    await newMovie.save();

    //Assert
    expect(await MovieModule.canUserAddMovies("12323")).to.be.true;
  });
});
