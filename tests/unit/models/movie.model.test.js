const { expect } = require("chai");
const e = require("express");
const MovieModel = require("../../../src/models/movie.model");
const MovieSchema = require("../../../src/schemas/movie.schema");

describe("movie model tests", function () {
  afterEach(async () => {
    await MovieSchema.remove();
  });

  it("should successfully return movie object", function () {
    //Arrange
    const movie = {
      Title: "title",
      Released: new Date(),
      Genre: "genre",
      Director: "director",
    };
    const userID = "12323";

    //Act
    const movieObject = new MovieModel(movie, userID).getMovie();

    //Assert
    expect(movieObject.title).to.equal(movie.Title);
    expect(movieObject.genre).to.equal(movie.Genre);
    expect(movieObject.director).to.equal(movie.Director);
    expect(movieObject.createdBy).to.equal(userID);
    expect(movieObject.released.getFullYear()).to.equal(
      movie.Released.getFullYear()
    );
    expect(movieObject.released.getMonth()).to.equal(movie.Released.getMonth());
    expect(movieObject.released.getDay()).to.equal(movie.Released.getDay());
  });
});
