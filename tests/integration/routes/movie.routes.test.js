const MovieSchema = require("../../../src/schemas/movie.schema");
const chai = require("chai");
const chaiHttp = require("chai-http");
const TokenBuilder = require("../../builder/token.builder");
const { expect } = require("chai");
const mock = require("mock-require");
chai.use(chaiHttp);

mock(
  "../../../src/modules/omdbapi.module.js",
  "../../mocks/omdbapi.module.mock.js"
);
describe("Saving movies route tests", function () {
  afterEach(async () => {
    await MovieSchema.remove();
  });

  it("should successfully save new movie", async () => {
    //Assert
    const user = {
      id: "123",
      role: "basic",
      name: "Basic Thomas",
      username: "basic-thomas",
      password: "sR-_pcoow-27-6PAwCD8",
    };
    const server = require("../../../server");
    const token = TokenBuilder.getToken(user);

    //Act
    const response = await chai
      .request(server)
      .post("/movies")
      .set("authorization", token)
      .send({ movieTitle: "little" });

    //Assert
    const movie = await MovieSchema.findOne({ title: "Little Miss Sunshine" });
    const savedMovieDate = new Date(movie.released);
    const now = new Date();
    expect(response.status).to.equal(200);
    expect(movie.title).to.equal("Little Miss Sunshine");
    expect(movie.genre).to.equal("Comedy, Drama");
    expect(movie.director).to.equal("Jonathan Dayton, Valerie Faris");
    expect(savedMovieDate.getFullYear()).to.equal(now.getFullYear());
    expect(savedMovieDate.getDay()).to.equal(now.getDay());
    expect(savedMovieDate.getMonth()).to.equal(now.getMonth());
  });

  it("should return 400 if there is no token", async () => {
    //Assert
    const server = require("../../../server");

    //Act
    const response = await chai
      .request(server)
      .post("/movies")
      .send({ movieTitle: "little" });

    //Assert
    expect(response.status).to.equal(400);
  });

  it("should return 401 if token is invalid", async () => {
    //Assert
    const server = require("../../../server");

    //Act
    const response = await chai
      .request(server)
      .post("/movies")
      .set("authorization", "notReallyAToken")
      .send({ movieTitle: "little" });

    //Assert
    expect(response.status).to.equal(401);
  });

  it("should return 404 if there is no movie with requested title", async () => {
    //Assert
    const user = {
      id: "123",
      role: "basic",
      name: "Basic Thomas",
      username: "basic-thomas",
      password: "sR-_pcoow-27-6PAwCD8",
    };
    const server = require("../../../server");
    const token = TokenBuilder.getToken(user);

    //Act
    const response = await chai
      .request(server)
      .post("/movies")
      .set("authorization", token)
      .send({ movieTitle: "ADSSDADSSSSD" });

    //Assert
    expect(response.status).to.equal(404);
  });
  it("should successfully let save more than five movies if user role is different than basic", async () => {
    //Arrange
    const movie = {
      title: "title",
      released: new Date(),
      genre: "genre",
      director: "director",
      createdBy: 123,
    };

    for (let i = 0; i < 5; i++) {
      const newMovie = new MovieSchema(movie);
      await newMovie.save();
    }

    const user = {
      id: 123,
      role: "premium",
      name: "Basic Thomas",
      username: "basic-thomas",
      password: "sR-_pcoow-27-6PAwCD8",
    };
    const server = require("../../../server");
    const token = TokenBuilder.getToken(user);

    //Act
    const response = await chai
      .request(server)
      .post("/movies")
      .set("authorization", token)
      .send({ movieTitle: "little" });

    //Assert
    expect(response.status).to.equal(200);
  });

  it("should return 403 if user already saved 5 movies in one month", async () => {
    //Arrange
    const movie = {
      title: "title",
      released: new Date(),
      genre: "genre",
      director: "director",
      createdBy: 123,
    };

    for (let i = 0; i < 5; i++) {
      const newMovie = new MovieSchema(movie);
      await newMovie.save();
    }

    const user = {
      id: 123,
      role: "basic",
      name: "Basic Thomas",
      username: "basic-thomas",
      password: "sR-_pcoow-27-6PAwCD8",
    };
    const server = require("../../../server");
    const token = TokenBuilder.getToken(user);

    //Act
    const response = await chai
      .request(server)
      .post("/movies")
      .set("authorization", token)
      .send({ movieTitle: "little" });

    //Assert
    expect(response.status).to.equal(403);
  });
});

describe("Get list of movies for user test", function () {
  afterEach(async () => {
    await MovieSchema.remove();
  });

  it("should successfully return list of movies", async () => {
    //Arrange
    const movie = {
      title: "title",
      released: new Date(),
      genre: "genre",
      director: "director",
      createdBy: "123",
    };
    const movie2 = {
      title: "other_title",
      released: new Date(),
      genre: "other_genre",
      director: "other_director",
      createdBy: "123",
    };
    const newMovie = new MovieSchema(movie);
    await newMovie.save();
    const newMovie2 = new MovieSchema(movie2);
    await newMovie2.save();

    const user = {
      id: "123",
      role: "basic",
      name: "Basic Thomas",
      username: "basic-thomas",
      password: "sR-_pcoow-27-6PAwCD8",
    };
    const server = require("../../../server");
    const token = TokenBuilder.getToken(user);

    //Act
    const response = await chai
      .request(server)
      .get("/movies")
      .set("authorization", token)
      .send();

    //Assert
    expect(response.status).to.equal(200);
    expect(response.body[0].title).to.equal(movie.title);
    expect(response.body[0].genre).to.equal(movie.genre);
    expect(response.body[0].director).to.equal(movie.director);
    expect(response.body[0].createdBy).to.equal(movie.createdBy);
    expect(response.body[1].title).to.equal(movie2.title);
    expect(response.body[1].genre).to.equal(movie2.genre);
    expect(response.body[1].director).to.equal(movie2.director);
    expect(response.body[1].createdBy).to.equal(movie2.createdBy);
  });

  it("should return 404 if user did not add any movies", async () => {
    //Arrange
    const movie = {
      title: "title",
      released: new Date(),
      genre: "genre",
      director: "director",
      createdBy: "123",
    };
    const movie2 = {
      title: "other_title",
      released: new Date(),
      genre: "other_genre",
      director: "other_director",
      createdBy: "123",
    };
    const newMovie = new MovieSchema(movie);
    await newMovie.save();
    const newMovie2 = new MovieSchema(movie2);
    await newMovie2.save();

    const user = {
      id: "54343",
      role: "basic",
      name: "Basic Thomas",
      username: "basic-thomas",
      password: "sR-_pcoow-27-6PAwCD8",
    };

    const server = require("../../../server");
    const token = TokenBuilder.getToken(user);

    //Act
    const response = await chai
      .request(server)
      .get("/movies")
      .set("authorization", token)
      .send();

    //Assert
    expect(response.status).to.equal(404);
  });

  it("should return 400 if there is no token inside request", async () => {
    //Arrange
    const server = require("../../../server");

    //Act
    const response = await chai.request(server).get("/movies").send();

    //Assert
    expect(response.status).to.equal(400);
  });

  it("should return 401 if token is invalid", async () => {
    //Assert
    const server = require("../../../server");

    //Act
    const response = await chai
      .request(server)
      .get("/movies")
      .set("authorization", "notReallyAToken")
      .send();

    //Assert
    expect(response.status).to.equal(401);
  });
});
