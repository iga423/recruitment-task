const jwt = require("jsonwebtoken");

class TokenBuilder {
  static getToken(user) {
    return jwt.sign(
      {
        userId: user.id,
        name: user.name,
        role: user.role,
      },
      process.env.JWT_SECRET,
      {
        issuer: "https://www.netguru.com/",
        subject: `${user.id}`,
        expiresIn: 30 * 60,
      }
    );
  }
}
module.exports = TokenBuilder;
