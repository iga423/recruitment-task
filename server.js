const express = require("express");
const mongoose = require("mongoose");

const databaseName = process.env.MONGO_DATABASE_NAME;
const databaseHost = process.env.MONGO_DATABASE_HOST;
const urlToConnectWithMongo = `mongodb://${databaseHost}:27017/${databaseName}`;

const app = express();
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.get("/", (req, res) => {
  res.send("Hello World!");
});

require("./src/routes/movie.routes")(app);
module.exports = app.listen(process.env.PORT, async () => {
  try {
    await mongoose.connect(urlToConnectWithMongo, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  } catch (error) {
    console.error(error);
  }

  console.log(`$$ Connected with MongoDb - ${databaseName}`);
  console.log(`Server listening at http://localhost:${process.env.PORT}`);
});
