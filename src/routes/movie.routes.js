const MovieModule = require("../modules/movie.module");
routes = (app) => {
  app.post("/movies", async (req, res) => {
    const status = await MovieModule.saveNewMovie(
      req.body.movieTitle,
      req.headers.authorization
    );

    res.sendStatus(status);
  });

  app.get("/movies", async (req, res) => {
    const response = await MovieModule.getUserMovies(req.headers.authorization);
    res.status(response.status).send(response.message);
  });
};
module.exports = routes;
