const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const movieSchema = new Schema(
  {
    title: {
      type: String,
    },
    released: {
      type: Date,
    },
    genre: {
      type: String,
    },
    director: {
      type: String,
    },

    createdBy: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("movie", movieSchema, "movies");
