const got = require("got");

class OmdbApiModule {
  static async getMovieInfo(movieTitle) {
    const requestString = `http://www.omdbapi.com/?t=${movieTitle}&apikey=${process.env.API_KEY}`;
    const response = await got(requestString);

    return JSON.parse(response.body);
  }
}
module.exports = OmdbApiModule;
