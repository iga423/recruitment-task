const MovieSchema = require("../schemas/movie.schema");
const OmdbApiModule = require("./omdbapi.module");
const MovieModel = require("../models/movie.model");
const jwt = require("jsonwebtoken");
class MovieModule {
  static async saveNewMovie(movieTitle, token) {
    if (!movieTitle || !token) {
      return 400;
    }

    let user;
    try {
      user = jwt.verify(token, process.env.JWT_SECRET);
    } catch (error) {
      console.error(error);
      return 401;
    }

    if (
      user.role === "basic" &&
      !(await MovieModule.canUserAddMovies(user.userId))
    ) {
      return 403;
    }

    try {
      const movie = await OmdbApiModule.getMovieInfo(movieTitle);

      if (movie.Response === "False") {
        return 404;
      }
      const movieModel = new MovieModel(movie, user.userId);
      const newMovie = new MovieSchema(movieModel.getMovie());
      await newMovie.save();

      return 200;
    } catch (error) {
      console.error(error);
      return 500;
    }
  }

  static async canUserAddMovies(id) {
    const today = new Date();
    const monthBeginning = new Date(today.getFullYear(), today.getMonth(), 1);

    const movies = await MovieSchema.find({
      createdBy: id,
      createdAt: { $gte: monthBeginning },
    });

    if (movies && movies.length >= 5) {
      return false;
    }
    return true;
  }

  static async getUserMovies(token) {
    if (!token) {
      return { status: 400 };
    }

    let user;
    try {
      user = jwt.verify(token, process.env.JWT_SECRET);
    } catch (error) {
      console.error(error);
      return { status: 401 };
    }

    const movies = await MovieSchema.find({ createdBy: user.userId });

    if (movies.length === 0) {
      return { status: 404 };
    } else return { status: 200, message: movies };
  }
}
module.exports = MovieModule;
