class MovieModel {
  constructor(movie, id) {
    this.title = movie.Title;
    this.released = movie.Released;
    this.genre = movie.Genre;
    this.director = movie.Director;
    this.createdBy = id;
  }

  getMovie() {
    return this;
  }
}
module.exports = MovieModel;
